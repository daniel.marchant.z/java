package com.springboot.app.services;

import java.util.List;
import java.util.Optional;

import com.springboot.app.models.Usuario;

public interface IUsuarioService {

	List<Usuario>getAllUsuarios();
	Usuario guardarUsuario(Usuario usu);
	Optional<Usuario> getUsuarioById(Long id);
	Boolean eliminarUsuario(Usuario usu);
}
