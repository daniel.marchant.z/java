package com.springboot.app.services;


import java.util.List;
import java.util.Optional;

import com.springboot.app.models.Comuna;



public interface IComunaService {

	List<Comuna> getAllComunas();
	Comuna guardarComuna(Comuna com);
	Optional<Comuna> getComunaById(Long id);
	Boolean eliminarComuna(Comuna com);
	
	
}
