package com.springboot.app.services;

import java.util.List;
import java.util.Optional;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import com.springboot.app.dao.IUsuarioDao;
import com.springboot.app.models.Usuario;

@Service
public class IUsuarioServicesImpl implements IUsuarioService {

	@Autowired
	IUsuarioDao usuarioDao;
	
	@Override
	@Transactional(readOnly=true)
	public List<Usuario> getAllUsuarios() {
		
		//reglas de negocio
		return (List<Usuario>) usuarioDao.findAll();
	}

	@Override
	@Transactional
	public Usuario guardarUsuario(Usuario usu) {
		
		return usuarioDao.save(usu);
		
	}

	@Override
	@Transactional(readOnly = true)
	public Optional<Usuario> getUsuarioById(Long id) {
		
		return usuarioDao.findById(id);
	}

	@Override
	@Transactional
	public Boolean eliminarUsuario(Usuario usu) {
		
		try {
			usuarioDao.delete(usu);
			return true;
			
		}catch (Exception e) {
			System.out.println("error :" + e);
			
		}
		return false;
		
	}

	
}
