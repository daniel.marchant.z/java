package com.springboot.app.services;


import java.util.List;
import java.util.Optional;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.springboot.app.dao.IProductoDao;

import com.springboot.app.models.Producto;



@Service
public class IProductoServicesImpl implements IProductoService {

	@Autowired
	IProductoDao productoDao;

	@Override
	@Transactional(readOnly = true)
	public List<Producto> getAllProductos() {
		
		return (List<Producto>) productoDao.findAll();
	}

	@Override
	@Transactional
	public Producto guardarProducto(Producto prod) {
		
		return productoDao.save(prod);
	}

	@Override
	@Transactional(readOnly = true)
	public Optional<Producto> getProductoById(Long id) {
		
		return productoDao.findById(id);
	}

	@Override
	@Transactional
	public Boolean eliminarProducto(Producto prod) {
		
		try {
			productoDao.delete(prod);
			return true;
			
		}catch (Exception e) {
			System.out.println("error :" + e);
			
		}
		return false;
	}


	
	
	

	
	

	


	
}
