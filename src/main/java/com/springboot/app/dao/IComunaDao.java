package com.springboot.app.dao;

import org.springframework.data.repository.CrudRepository;

import com.springboot.app.models.Comuna;


public interface IComunaDao extends CrudRepository<Comuna, Long>{

}
