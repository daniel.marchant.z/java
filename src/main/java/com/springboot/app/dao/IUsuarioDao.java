package com.springboot.app.dao;

import org.springframework.data.repository.CrudRepository;

import com.springboot.app.models.Usuario;

public interface IUsuarioDao extends CrudRepository<Usuario, Long>{

}
