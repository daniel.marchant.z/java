package com.springboot.app.dao;



import org.springframework.data.repository.CrudRepository;

import com.springboot.app.models.Producto;



public interface IProductoDao extends CrudRepository<Producto, Long>{

}
