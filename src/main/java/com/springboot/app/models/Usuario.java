package com.springboot.app.models;

import java.io.Serializable;


import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.sun.istack.NotNull;

@Entity
@Table(name="usuarios")
public class Usuario implements Serializable{
	
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY )
	private Long id;
	//@Column(nombre="nombre_usuario")puedo cambiar el nombre que se vera en la base
	@NotNull
	private String nombre;
	private String apellido;
	private int edad;
	private String email;
	@NotNull
	private String password;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="comuna_id")
	@JsonIgnoreProperties({"hibernateLazyInitializer","handler","usuarios"})//arreglo de comuna no me traigas usuarios
	private Comuna comuna;													//por que usuarios al traer comuna originalmente viene con usuarios y se produce un bucle
																			//por que comuna trae nuevamente usuarios y asi sucesivamente
public Usuario() {
		
		
	}
	
	public Usuario(Long id, String nombre, String apellido, int edad, String email, String password, Comuna comuna) {
		
		this.id = id;
		this.nombre = nombre;
		this.apellido = apellido;
		this.edad = edad;
		this.email = email;
		this.password= password;
		this.comuna= comuna;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Comuna getComuna() {
		return comuna;
	}

	public void setComuna(Comuna comuna) {
		this.comuna = comuna;
	}

	
	private static final long serialVersionUID = 1L;
	
	
}
