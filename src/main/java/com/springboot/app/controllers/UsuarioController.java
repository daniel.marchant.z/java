package com.springboot.app.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.springboot.app.models.Usuario;
import com.springboot.app.services.IUsuarioService;

@RestController
@RequestMapping("/api")
public class UsuarioController {

	@Autowired
	private IUsuarioService usuarioServ;
	
	//obtener todos los usuarios
	@GetMapping("/usuarios")
	public List<Usuario> getAllusuarios(){
		return usuarioServ.getAllUsuarios();
		
	}
	
	//obtener usuario por id
	@GetMapping("/usuarios/{id}")
	public Optional<Usuario> getUsuarioById(@PathVariable Long id){
		return usuarioServ.getUsuarioById(id);
		
	}
	
	//guardar usuarios
	@PostMapping("/usuarios")
	public Usuario guardarUsuario(@RequestBody Usuario usu) {
		return usuarioServ.guardarUsuario(usu);
		
	}
	
	//actualizar usuarios
	@PutMapping("/usuarios")
	public Usuario actualizarUsuario(@RequestBody Usuario usu) {
		
		//aplicar validacion para que actualice solo si el registro existe
		
		Usuario usuarioActualizado = new Usuario();
		//Si no existe el id no sigue
		if(usu.getId()==null) {
			return null;
		}
		
		//existe el id del usuario 
		//validar que el id exista en la bd
		
		Optional<Usuario> usuBd = usuarioServ.getUsuarioById(usu.getId());
		
		if ( usuBd.isPresent()) {
			usuarioActualizado = usuarioServ.guardarUsuario(usu);
		}
		
		return usuarioActualizado;
		
		
	}
	
	//Eliminar usuario
	@DeleteMapping("/usuarios")
	public Boolean eliminarUsuario(@RequestBody Usuario usu) {
		
		return usuarioServ.eliminarUsuario(usu);
	}
}
